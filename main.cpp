//
// Disman
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

//
// This will normally get run for OpenXT based systems using this command line.
// disman --syslog
//

// ============================================================================
// Includes
// ============================================================================

#include <unistd.h>     // daemon, setsid
#include <signal.h>     // signal
#include <sys/stat.h>   // umask
#include <errno.h>      // errno
#include <cstdlib>      // std::exit
#include <QtCore>

#include <logging.h>
#include <disman.h>

// ============================================================================
// Definitions
// ============================================================================

typedef struct {
    QString configLocation;
    bool debuggingEnabled;
    bool syslogLoggingEnabled;
    bool foregroundEnabled;
    bool testModeEnabled;
} CmdLineOptions;

Q_DECLARE_METATYPE(uuid_t);
Q_DECLARE_METATYPE(domid_t);
Q_DECLARE_METATYPE(uint32_t);
Q_DECLARE_METATYPE(QList<dh_display_info>);

// ============================================================================
// Globals
// ============================================================================

CmdLineOptions cmdlineOptions;

// ============================================================================
// Implementation
// ============================================================================

void exitHandler(int signal) {
    qDebug().nospace() << "Received " << sys_siglist[signal] << " (" << signal << ") : " << strsignal(signal);
    qDebug() << "Exiting";
    QCoreApplication::quit();
}

/**
  create background process out of the application, source code taken from: http://www.enderunix.org/docs/eng/daemon.php
  with some minor modifications
  */
void init_daemon() {
    if (daemon(0,0)) {
        int err = errno;
        qWarning().nospace() << "Application failed to daemonize. Received errno (" << err << ") : " << strerror(err);
        qWarning() << "Exiting";
        QCoreApplication::exit(err);
    }

    /* child (daemon) continues */
    setsid(); /* obtain a new process group */
}

void parseCommandLine(QCoreApplication &app) {
    QCommandLineParser parser;
    parser.setApplicationDescription("display manager daemon");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption configOption(QStringList() << "c" << "config",
                                    "location of display configuration",
                                    "url", "db:///disman");
    parser.addOption(configOption);

    QCommandLineOption debugOption(QStringList() << "d" << "debug",
                                   "enable debug/verbose logging");
    parser.addOption(debugOption);

    QCommandLineOption syslogLoggingOption(QStringList() << "s" << "syslog",
                                    "use syslog logging instead of console");
    parser.addOption(syslogLoggingOption);

    // QCommandLineOption foregroundOption(QStringList() << "f" << "foreground",
    //                                     "run in foreground - do not fork");
    // parser.addOption(foregroundOption);

    QCommandLineOption testOption(QStringList() << "t" << "test",
                                    "test simulation mode");
    parser.addOption(testOption);

    parser.process(app);

    cmdlineOptions.configLocation = parser.value("config");
    cmdlineOptions.debuggingEnabled = parser.isSet(debugOption);
    cmdlineOptions.syslogLoggingEnabled = parser.isSet(syslogLoggingOption);
    // cmdlineOptions.foregroundEnabled = parser.isSet(foregroundOption);
    cmdlineOptions.testModeEnabled = parser.isSet(testOption);
}

int main(int argc, char *argv[]) {
    umask(027); /* set newly created file permissions */

    qInstallMessageHandler(Logging::logOutput);

    // Set stuff up for command line parsing
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("Disman");
    QCoreApplication::setApplicationVersion("2.0");

    // Root privileges are needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    qRegisterMetaType<uuid_t>("uuid_t");
    qRegisterMetaType<domid_t>("domid_t");
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<QList<dh_display_info>>("QList<dh_display_info>");

    // Parse command line
    parseCommandLine(app);

    // setup signal handlers
    signal(SIGCHLD,SIG_IGN);        /* ignore child */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGQUIT, exitHandler);   /* catch quit signal */
    signal(SIGHUP, exitHandler);    /* catch hangup signal */
    signal(SIGTERM, exitHandler);   /* catch kill signal */
    signal(SIGINT, exitHandler);    /* catch interrupt signal - Ctrl-C */

    // if (!cmdlineOptions.foregroundEnabled) {
    //     signal(SIGTSTP,SIG_IGN);        /* ignore tty signals - Ctrl-Z */

    //     init_daemon();
    // }

    if (Logging::logger()) {
        Logging::logger()->syslogMode = cmdlineOptions.syslogLoggingEnabled;
        Logging::logger()->debugMode = cmdlineOptions.debuggingEnabled;
    }

    qDebug() << "debugging enabled:"        << cmdlineOptions.debuggingEnabled;
    qDebug() << "syslog logging enabled:"   << cmdlineOptions.syslogLoggingEnabled;
    // qDebug() << "foreground enabled:"       << cmdlineOptions.foregroundEnabled;
    qDebug() << "config location:"          << cmdlineOptions.configLocation;
    qDebug() << "test mode enabled:"        << cmdlineOptions.testModeEnabled;

    Disman disman(cmdlineOptions.configLocation, cmdlineOptions.testModeEnabled);

    // Need to spin waiting for requests
    return app.exec();
}
