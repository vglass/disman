//
// Disman
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <memory>       // std::make_unique
#include <cstdint>      // uint32_t, int32_t
#include <stdexcept>    // std::invalid_argument, std::bad_alloc, std::domain_error, std::out_of_range
#include <unistd.h>     // getpid
#include <algorithm>    // std::min, std::max
#include <cstdlib>      // std::exit
#include <vector>       // vector
#include <QtCore>
#include <QRegion>
#include <QSize>
#include <QRect>
#include <json.hpp>
#include <QCoreApplication>

#include <disman.h>
#include <dbus.h>
#include <config.h>
#include <qdebug_helper.h>
#include <disman_adaptor.h>
#include <vglass_interface.h>
#include <dedicatedresource.h>
#include <db_interface.h>

#include <dbus_helpers.h>

// ============================================================================
// Implementation
// ============================================================================

// RCF - Might need more try/catch throughout this.
//     In addition to std::invalid_argument during parsing, nlohmann::json can
//     throw std::domain_error all over the place, std::bad_alloc during
//     construction, also std::out_of_range

Disman::Disman(QString configLocation, bool testModeEnabled) : mConfigLocation(configLocation), mTestModeEnabled(testModeEnabled),
                                                               mDismanReady(false), mTimer(nullptr) {
    using std::make_unique;

    // Root privileges are needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    // // Register our pid here, so the correct pid is in the pidfile, allowing our
    // // scripts to properly work.
    // QFile pidFile(PIDFILE);

    // if(pidFile.exists()) {
    //     qCritical().nospace() << "The PID file (" << PIDFILE << ") already exists. Either " << QCoreApplication::instance()->applicationName() << " is already running, or it crashed unexpectedly.";
    //     std::exit(-1);
    // }

    // pid_t pid = getpid();
    // if (pidFile.open(QIODevice::ReadWrite)) {
    //     QTextStream stream(&pidFile);
    //     stream << QString::number((unsigned int)pid) << endl;
    // } else {
    //     qWarning() << "Failed to write pid file. Perhaps SELinux is preventing writes to /var/run";
    //     std::exit(2);
    // }
    // pidFile.close();

    db = std::make_unique<db_dbus_t>(QString("com.citrix.xenclient.db"),
                                     QString("/"),
                                     QDBusConnection::systemBus());
    dbus = make_unique<DBus>();
    config = make_unique<Config>();
    mDBusListener = make_dbus_listener();
    mTimer = new QTimer(this);
    mTimer->setSingleShot(true);
    connect(mTimer, &QTimer::timeout, this, &Disman::handle_hotplug);

    registerDbus();

    refresh(false);
}

Disman::~Disman() {
    // QFile::remove(PIDFILE);

    if (mDismanReady) {
        nxus_deinit();

        disconnect(mDBusListener.get(), &dbus_listener_t::guest_deleted, this, &Disman::guest_deleted);

        unregisterDbus();
    }

    mTimer->stop();
}

void Disman::registerDbus() {
    if (!dbus->registerService<DismanAdaptor>(this, "com.openxt.disman") ||
        !dbus->registerObject(this, "/com/openxt/disman")) {
        std::exit(3);
    }
}

void Disman::unregisterDbus() {
    dbus->unregisterObject("/com/openxt/disman");
    dbus->unregisterService("com.openxt.disman");
}


//
// IVC / NXUS Stuff
//

void Disman::nxus_init() {
    if (!mTestModeEnabled) {
        connect(mDBusListener.get(), &dbus_listener_t::guest_started, this, &Disman::guest_started);
        connect(mDBusListener.get(), &dbus_listener_t::guest_stopped, this, &Disman::guest_stopped);
    }

    try {
        // Fake it for all already running VMs
        for (auto& uuid : mDBusListener->get_vm_list()) {
            if (uuid != "00000000-0000-0000-0000-000000000001") {
                guest_started(uuid);
            }
        }
    } catch (const std::runtime_error &e) {
        qDebug() << e.what();
    }
}

void Disman::nxus_deinit() {
    if (!mTestModeEnabled) {
        disconnect(mDBusListener.get(), &dbus_listener_t::guest_started, this, &Disman::guest_started);
        disconnect(mDBusListener.get(), &dbus_listener_t::guest_stopped, this, &Disman::guest_stopped);

        for(auto& uuid: mDedicatedResources.keys()) {
            guest_stopped(uuid);
        }
    }
}

void Disman::guest_started(const uuid_t &uuid) {
    using std::shared_ptr;
    using std::make_shared;

    qDebug() << "Guest started -" << uuid.toString().mid(1,uuid.toString().length()-2);
    bool has_gpu = false;
    try {
        has_gpu = !mDBusListener->get_gpu(uuid).isEmpty();
    } catch (const std::runtime_error &e) {
        qDebug() << e.what();
    }
    if (has_gpu) {
        if (mConfig.empty()) {
            qWarning() << "Disman::guest_started() called before disman was ready.";
            return;
        }

        if (mDedicatedResources.contains(uuid)) {
            qDebug() << "Guest has a dedicated GPU, but IVC connection already exists";
        } else {
            qDebug() << "Guest has a dedicated GPU, initializing IVC connection";
            try {
                domid_t domid = mDBusListener->get_domid(uuid);

                shared_ptr<DedicatedResource> dedicatedResource = make_shared<DedicatedResource>(uuid, domid);
                mDedicatedResources.insert(uuid, dedicatedResource);

                // connect everything
                connect(dedicatedResource.get(), &DedicatedResource::displays_changed, this, &Disman::displays_changed);
            } catch (const std::runtime_error &e) {
                qWarning() << e.what();
            }
        }
    }
}

void Disman::guest_stopped(const uuid_t &uuid) {
    using std::shared_ptr;

    qDebug() << "Guest stopped -" << uuid.toString().mid(1,uuid.toString().length()-2);

    if (mConfig.empty()) {
        qWarning() << "Disman::guest_stopped() called before disman was ready.";
        return;
    }

    if (mDedicatedResources.contains(uuid)) {
        shared_ptr<DedicatedResource> dedicatedResource = mDedicatedResources.take(uuid);
        // disconnect everything
        disconnect(dedicatedResource.get(), &DedicatedResource::displays_changed, this, &Disman::displays_changed);
    }

    // Loop through gpus to find and remove the gpu config cooresponding to this uuid
    for (uint32_t i = 0; i < mConfig["gpus"].size(); ++i) {
        QString bdf = mDBusListener->get_gpu(uuid); // long form bdf - 0000:00:02.0
        if (mConfig["gpus"][i]["name"] == (QString("PT_")+bdf).toStdString()) {
            qDebug() << "Removing" << qPrintable(QString("PT_")+bdf);
            mConfig["gpus"].erase(i);
            json j = mConfig;
            loadConfig(j);
            refresh(j, true);
            break;
        }
    }
}

void Disman::guest_deleted(const uuid_t &uuid) {
    using std::string;

    for (auto& gpu : mConfig["gpus"]) {
        if (gpu["desktop_type"].get<string>() == "dedicated") { continue; }
        for (auto& display : gpu["displays"]) {
            if (QString::fromStdString(display["uuid"].get<string>()) == uuid.toString().mid(1,uuid.toString().length()-2)) {
                refresh(false);
                return;
            }
        }
    }
}

void Disman::displays_changed(const uuid_t& uuid, const QList<dh_display_info>& display_info, const uint32_t size) {
    using std::string;

    if (size == 0) {
        return;
    }

    qDebug() << "Received updated display information for" << uuid;

    json j = mConfig;
    QString bdf = mDBusListener->get_gpu(uuid); // long form bdf - 0000:00:02.0
    QString gpu_name = QString("PT_")+bdf;
    json new_gpu;

    // Find gpu if it already exists, copy it and remove it
    for (uint32_t i = 0; i < j["gpus"].size(); ++i) {
        if (QString::fromStdString(j["gpus"][i]["name"]) == gpu_name) {
            new_gpu = j["gpus"].at(i);
            j["gpus"].erase(i);
            break;
        }
    }

    // If it didn't exist, fill out the gpu fields
    if (new_gpu.empty()) {
        qDebug().nospace() << "Adding new gpu (" << qPrintable(gpu_name) << ")";
        // Create gpu object with the information sent
        new_gpu["name"] = gpu_name.toStdString();
        new_gpu["device_path"] = bdf.toStdString();
        new_gpu["desktop_type"] = "dedicated";
        new_gpu["renderable"] = false;
        new_gpu["displays"] = {};
    }

    // Remove any displays that have gone away
    if (new_gpu["displays"].size() > 0) {
        for (int32_t i = new_gpu["displays"].size()-1; i >= 0; --i) {
            bool found = false;
            string key = new_gpu["displays"].at(i)["name"].get<string>().substr(2);
            for (uint32_t i2 = 0; i2 < size; ++i2) {
                if (display_info[i2].key & DISCRETE_MASK) {
                    if (QString::number(display_info[i2].key).toStdString() == key) {
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                qDebug().nospace() << "Removing display (D_" << qPrintable(QString::fromStdString(key)) << ")";
                new_gpu["displays"].erase(i);
            }
        }
    }

    QString new_uuid = uuid.toString().mid(1,uuid.toString().length()-2);

    // update/add all others - at least enough to calculate hashes
    for (uint32_t i2 = 0; i2 < size; ++i2) {
        if (display_info[i2].key & DISCRETE_MASK) {
            json display;
            // Find display if it already exists, copy it and remove it
            if (new_gpu["displays"].size() > 0) {
                for (uint32_t i = 0; i < new_gpu["displays"].size(); ++i) {
                    if (QString::number(display_info[i2].key).toStdString() == new_gpu["displays"].at(i)["name"].get<string>().substr(2)) {
                        display = new_gpu["displays"].at(i);
                        new_gpu["displays"].erase(i);
                        break;
                    }
                }
            }

            // If it didn't exist, fill out the display fields
            if (display.empty()) {
                qDebug().nospace() << "Adding display (D_" << qPrintable(QString::number(display_info[i2].key)) << ")";
                display["name"] = (QString("D_") + QString::number(display_info[i2].key)).toStdString();
                display["preferred_mode_index"] = 0;
                display["modes"] = {};
                display["x"] = MAX_NEG_INT;
                display["y"] = MAX_NEG_INT;
            } else {
                qDebug().nospace() << "Updating display (D_" << qPrintable(QString::number(display_info[i2].key)) << ")";
                display["modes"].clear();
            }
            display["edid_hash"] = QString::number(calculateDisplayHash(display),16).toStdString();
            new_gpu["displays"].push_back(display);
        }
    }

    new_gpu["config_hash"] = QString::number(calculateGpuHash(new_gpu),16).toStdString();

    // Add new gpu config to list of gpus
    if (new_gpu["displays"].size() > 0) {
        qDebug() << "Adding" << new_gpu["name"];
        j["gpus"].push_back(new_gpu);
    } else {
        qDebug() << new_gpu["name"] << "has no displays.";
    }

    loadConfig(j);

    // Now that we've loaded the stored config, make sure position, uuid and mode are correct
    for (uint32_t i2 = 0; i2 < size; ++i2) {
        if (display_info[i2].key & DISCRETE_MASK) {
            QString display_name  = QString("D_") + QString::number(display_info[i2].key);
            for (auto& gpu : j["gpus"]) {
                if (gpu["name"].get<string>() == gpu_name.toStdString()) {
                    for (auto& display : gpu["displays"]) {
                        if (display["name"].get<string>() == display_name.toStdString()) {
                            if (display.count("x") == 0) {
                                display["x"] = display_info[i2].x;
                            }
                            if (display.count("y") == 0) {
                                display["y"] = display_info[i2].y;
                            }

                            if (display.count("uuid")) {
                                QString old_uuid = QString::fromStdString(display["uuid"].get<string>());
                                if (old_uuid != new_uuid) {
                                    qWarning() << "Detected a VM trying to use the same PT GPU as one we've seen before.";
                                    qWarning() << "This could have been caused by having two current VMs configured to use the";
                                    qWarning() << "same PT GPU, or because the VM was replaced.";
                                    qWarning() << "Will try to fix the NXus config.";
                                    qWarning().nospace() << "The old UUID is " << qPrintable(old_uuid) << ".";
                                    qWarning().nospace() << "The new UUID is " << qPrintable(new_uuid) << ".";
                                }
                            }
                            display["uuid"] = new_uuid.toStdString();
                            display["modes"].clear();
                            json mode;
                            mode["name"] = (QString::number(display_info[i2].width) + "x" + QString::number(display_info[i2].height)).toStdString();
                            mode["width"] = display_info[i2].width;
                            mode["height"] = display_info[i2].height;
                            display["modes"].push_back(mode);
                        }
                    }
                }
            }
        }
    }

    refresh(j);
}

//
// Helper methods
//

// Calculate disman hash

// compares the hashes of two gpus
bool gpu_hash_comp(json &l, json &r) {
    return l["config_hash"] < r["config_hash"];
}

// compares the hashes of two displays
bool display_hash_comp(json &l, json &r) {
    return l["edid_hash"] < r["edid_hash"];
}

uint32_t Disman::calculateDismanHash(json &disconfig) {
    std::string disman_hash;

    std::sort(disconfig["gpus"].begin(), disconfig["gpus"].end(), gpu_hash_comp);
    for (auto gpu : disconfig["gpus"]) {
        disman_hash += gpu["config_hash"];
        std::sort(gpu["displays"].begin(), gpu["displays"].end(), display_hash_comp);
        for (auto display : gpu["displays"]) {
            disman_hash += display["edid_hash"];
        }
    }

    return qHash(QString::fromStdString(disman_hash));
}

// Calculate gpu hash
uint32_t Disman::calculateGpuHash(json &gpu) {
    std::string config_hash;

    std::sort(gpu["displays"].begin(), gpu["displays"].end(), display_hash_comp);
    for (auto display : gpu["displays"]) {
        config_hash += display["name"];
        config_hash += display["edid_hash"];
    }

    return qHash(QString::fromStdString(config_hash));
}

// Calculate display hash
uint32_t Disman::calculateDisplayHash(json &display) {
    return qHash(QString::fromStdString(display["name"]));
}

void Disman::loadConfig(json &disconfig) {
    using std::string;

    // Calculate disman hash (this is how we find the configuration again)
    uint32_t disman_hash = calculateDismanHash(disconfig);
    QString url = mConfigLocation+"/"+QString::number(disman_hash, 16);
    if (!mTestModeEnabled) {
        if (config->exists(QUrl(url))) {
            // Old configuration found
            for (auto& gpu : disconfig["gpus"]) {
                gpu = config->load(QUrl(url+"/"+QString::fromStdString(gpu["config_hash"])));
            }
        }
    }
}

// Send it to the UIVM and vglass
bool Disman::saveConfig(json &disconfig, const bool mustRespond) {
    using std::string;

    if (validateConfig(disconfig, "schema/disconfig.json")) {
        // Calculate disman hash (this is how we find the configuration again)
        QString disman_hash = QString::number(calculateDismanHash(disconfig), 16);
        // Loop through gpus saving non-dedicated gpu configs by config hash
        for (auto& gpu : disconfig["gpus"]) {
            QString config_hash = QString::fromStdString(gpu["config_hash"]);
            if (!mTestModeEnabled) {
                config->save(QUrl(mConfigLocation+"/"+disman_hash+"/"+config_hash), gpu);
            }
        }
        db->write("/disman/current", disman_hash);
        if (mustRespond || disconfig != mConfig) {
            // Send it to the UIVM and vglass
            emit configChanged(QString::fromStdString(disconfig.dump()));
            mConfig = disconfig;
        }
        return true;
    } else {
        qDebug() << "The config failed the schema validation.";
        qDebug() << disconfig.dump();
        return false;
    }
}

QString Disman::getRandomConfig() {
    QString configStr;

    qsrand(QDateTime::currentMSecsSinceEpoch() / 1000);

    QDir dir("examples/");
    if (!dir.exists()) {
        dir.setPath("/usr/share/disman/examples/");
    }
    QStringList filters = (QStringList() << "*.json");
    dir.setFilter( QDir::Files | QDir::Readable | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    dir.setNameFilters(filters);
    int total_files = dir.count();

    if (total_files > 0) {
        QString fileName = dir.path() + "/" + dir[(qrand() % total_files)];
        qDebug() << "Loading" << fileName;
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream in(&file);
            configStr = in.readAll();
        }
    } else {
        qDebug() << "No example files found.";
    }

    return configStr;
}

json Disman::getConfig() {
    using std::string;
    using std::make_unique;
    using std::unique_ptr;

    unique_ptr<vglass_dbus_t> vglass = make_unique<vglass_dbus_t>(QString("com.openxt.vglass"),
                                                                  QString("/com/openxt/vglass"),
                                                                  dbus->systemBus());

    if (vglass == nullptr || !vglass->isValid()) {
        qWarning() << "VGlass not found, cannot complete this request.";
        return json();
    }

    QString configStr;
    if (mTestModeEnabled) {
        configStr = getRandomConfig();
    } else {
        configStr = vglass->getConfig();
    }

    json disconfig = Config::parse(configStr);

    if (disconfig.empty()) { return disconfig; }

    // Loop through old gpus looking for dedicated gpus from nxus, and add them
    // to what's in disconfig
    if (!mConfig.empty()) {
        for (auto& gpu : mConfig["gpus"]) {
            if (gpu["desktop_type"].get<string>() == "dedicated") {
                disconfig["gpus"].push_back(gpu);
            }
        }
    }

    // maintain banner height
    if (db->exists("/disman/banner_height")) {
        bool success;

        QString str = db->read("/disman/banner_height");
        uint32_t value = str.toUInt(&success, 10);
        if (success) {
            disconfig["banner_height"] = value;
        }
    }

    // maintain frame time
    if (db->exists("/disman/frame_time")) {
        bool success;

        QString str = db->read("/disman/frame_time");
        uint32_t value = str.toUInt(&success, 10);
        if (success) {
            disconfig["frame_time"] = value;
        }
    }

    loadConfig(disconfig);

    return disconfig;
}


//
// DBus methods
//


bool Disman::saveConfig(const QString &configStr, QString &result) {
    qDebug() << "Disman : saveConfig -" << configStr;

    bool rv = false;

    json disconfig = Config::parse(configStr);

    if (!disconfig.empty()) {
        rv = refresh(disconfig);
    }

    if (!rv) {
        qDebug() << "Disman : saveConfig - failed.";
        result = QString();
    } else {
        result = QString::fromStdString(mConfig.dump());
    }

    return rv;
}

// RCF - Is it possible that this can get called before mConfig is valid?
QString Disman::loadConfig(void) {
    if (mConfig.empty()) {
        qWarning() << "Disman::loadConfig() called before disman was ready.";
        return "";
    }
    qDebug() << "Disman : loadConfig" << mConfig;

    return QString::fromStdString(mConfig.dump());
}

int32_t Disman::getBannerHeight() const {
    if (mConfig.empty()) {
        qWarning() << "Disman::getBannerHeight() called before disman was ready.";
        return -1;
    }

    return mConfig["banner_height"];
}

void Disman::setBannerHeight(const uint32_t value) {
    if (!mConfig.empty()) {
        uint32_t val = std::min(50u, std::max(0u, value));

        if (mConfig["banner_height"] != val) {
            mConfig["banner_height"] = val;
            db->rm("/disman/banner_height");
            db->write("/disman/banner_height", QString::number(val, 10));

            // Send it to the UIVM and vglass
            emit configChanged(QString::fromStdString(mConfig.dump()));
        }
    } else {
        qWarning() << "Disman::setBannerHeight() called before disman was ready.";
    }
}

int32_t Disman::getFrameTime() const {
    if (mConfig.empty()) {
        qWarning() << "Disman::getFrameTime() called before disman was ready.";
        return -1;
    }

    return mConfig["frame_time"];
}

void Disman::setFrameTime(const uint32_t value) {
    if (!mConfig.empty()) {
        uint32_t val = std::min(2000u, std::max(7u, value));

        if (mConfig["frame_time"] != val) {
            mConfig["frame_time"] = val;
            db->rm("/disman/frame_time");
            db->write("/disman/frame_time", QString::number(val, 10));

            // Send it to the UIVM and vglass
            emit configChanged(QString::fromStdString(mConfig.dump()));
        }
    } else {
        qWarning() << "Disman::setFrameTime() called before disman was ready.";
    }
}

void Disman::hotplug(void) {
    qDebug() << "Disman : Hotplug event";

    if (!mTimer->isActive()) {
        qDebug() << "Disman : starting timer";
        mTimer->start(50);
    }
}

void Disman::refresh(const bool mustRespond) {
    json disconfig = getConfig();

    if (!disconfig.empty()) {
        if (refresh(disconfig, mustRespond) && !mDismanReady) {
            connect(mDBusListener.get(), &dbus_listener_t::guest_deleted, this, &Disman::guest_deleted);

            nxus_init();

            mDismanReady = true;
        }
    }

    if (mTestModeEnabled) {
        std::exit(1);
    }
}

void Disman::handle_hotplug() {
    qDebug() << "Disman : Handling Hotplug event";

    refresh(true);
}

void Disman::revertToClonedMode() {
    qDebug() << "Disman : revertToClonedMode";

    json disconfig = mConfig;

    for (auto& gpu : disconfig["gpus"]) {
        if (gpu["desktop_type"].get<std::string>() == "shared") {
            gpu["desktop_type"] = "cloned";
        }
    }

    refresh(disconfig, false);
}

void Disman::removeSavedConfigs(QString hash) const {
    qDebug() << "Disman : removeSavedConfigs :" << hash;

    if (hash.isEmpty() || hash == "banner_height" ||
                          hash == "frame_time" || hash.contains('/')) {
        config->remove(QUrl(mConfigLocation));
    } else {
        config->remove(QUrl(mConfigLocation+"/"+hash));
    }
}

void Disman::exit() {
    std::exit(0);
}

//
// Validation/Sanitation methods
//

QString Disman::debugDump(const json &disconfig, const QString tag) const {
    qDebug() << "******* DISMAN" << tag << "*******";
    qDebug() << disconfig.dump(4);
    qDebug() << "******* DISMAN" << tag << "*******";
    return QString::fromStdString(disconfig.dump());
}

// The refresh function starts everything from scratch.
//
// 1 - validate/sanitize the configuration.
// 2 - save the new configuration to db/disk.
//
// This will make sure that whatever config we have now, will remain
// the same unless something else configures it differently.
bool Disman::refresh(json &disconfig, const bool mustRespond) {
    if (disconfig.empty()) { return false; }
    if (!validateConfig(disconfig, "schema/disconfig_loose.json")) {
        qDebug() << "The config failed the schema validation.";
        qDebug() << disconfig.dump(4);
        return false;
    }

    debugDump(disconfig, "PRE-REFRESH");

    // Don't bother with this if the total number of GPUs is 0. This should
    // only happen during a unit test, but it's a good thing for safety too.
    if(disconfig.count("gpus") == 1 && disconfig["gpus"].size() != 0) {
        QString prev;
        if (mTestModeEnabled) {
            prev = QString::fromStdString(disconfig.dump());
        }
        fix_InvalidPinned(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_InvalidPinned");
        }
        fix_MissingShared(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_MissingShared");
        }
        fix_InvalidMode(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_InvalidMode");
        }
        filter_OverlappingDisplays(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_OverlappingDisplays");
        }
        filter_OrphanedDisplays(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_OrphanedDisplays");
        }
        //filter_SplitDesktop(disconfig);
        fix_NonOriginDesktop(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_NonOriginDesktop");
        }
        fix_InvalidPositions(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_InvalidPositions");
        }
        fix_InvalidCloned(disconfig);
        if (mTestModeEnabled && prev.compare(QString::fromStdString(disconfig.dump())) != 0) {
            prev = debugDump(disconfig, "fix_InvalidCloned");
        }
    }

    debugDump(disconfig, "POST-REFRESH");

    return saveConfig(disconfig, mustRespond);
}

bool Disman::modeCmp(const json &a, const json &b) {
    return a["width"].get<int>() < b["width"].get<int>() ||
           (a["width"].get<int>() == b["width"].get<int>() && a["height"].get<int>() < b["height"].get<int>());
}

QPoint Disman::getDisplayPosition(json &display) const {
    using std::domain_error;

    try {
        return QPoint(display["x"],display["y"]);
    } catch (domain_error& e) {
        // This can't happen
        return INVALIDPOINT;
    }
}

QSize Disman::getDisplayResolution(json &display) const {
    using std::domain_error;

    try {
        int32_t preferred_mode_index = display["preferred_mode_index"];
        json mode = display["modes"].at(preferred_mode_index);
        return QSize(mode["width"], mode["height"]);
    } catch (domain_error& e) {
        // This can't happen
        return INVALIDSIZE;
    }
}

// Make sure that we always have at least one unpinned/undedicated display
void Disman::fix_MissingShared(json &disconfig) const {
    using std::domain_error;
    using std::string;

    for (auto& gpu : disconfig["gpus"]) {
        if (gpu["desktop_type"].get<string>() == "dedicated") { continue; }
        if (gpu["desktop_type"].get<string>() == "cloned") { return; }
        if (gpu["desktop_type"].get<string>() == "shared") {
            for (auto& display : gpu["displays"]) {
                if (display["uuid"].get<string>() == "none") {
                    return;
                }
            }
            try {
                qDebug() << "No available display found for UIVM!!!";
                gpu["displays"].at(0)["uuid"] = "none";
            } catch (domain_error& e) {
                qDebug() << e.what();
            }
        }
    }
}

// Fix invalid cloned displays - This unpins displays and sets all positions equal.
void Disman::fix_InvalidCloned(json &disconfig) const {
    using std::string;

    for (auto& gpu : disconfig["gpus"]) {
        if (gpu["desktop_type"].get<string>() != "cloned") { continue; }

        int32_t x = 0;
        int32_t y = 0;
        for (unsigned int i = 0; i < gpu["displays"].size(); ++i) {
            auto& display = gpu["displays"].at(i);
            QString uuid = QString::fromStdString(display["uuid"].get<string>());
            if (uuid != "none") {
                qDebug() << "Display has a VM (" << qPrintable(uuid) << ") pinned to it, but desktop type is cloned.";
                display["uuid"] = "none";
            }
            if (0 == i) {
                x = display["x"];
                y = display["y"];
            } else {
                display["x"] = x;
                display["y"] = y;
            }
        }
    }
}

// Fix invalid pinned displays - This unpins displays that are pinned to a UUID
// that doesn't currently exist.
void Disman::fix_InvalidPinned(json &disconfig) const {
    using std::string;

    for (auto& gpu : disconfig["gpus"]) {
        if (gpu["desktop_type"].get<string>() == "dedicated") { continue; }
        for (auto& display : gpu["displays"]) {
            QString uuid = QString::fromStdString(display["uuid"].get<string>());
            if (uuid != "none") {
                try {
                    if (!mDBusListener->get_guest_vm_list().contains(uuid)) {
                        qDebug() << "Display has a VM (" << qPrintable(uuid) << ") pinned to it, but VM doesn't exist.";
                        display["uuid"] = "none";
                    }
                } catch (const std::runtime_error &e) {
                    qDebug() << e.what();
                }
            }
        }
    }
}

// Fix invalid mode - This makes sure that the preferred mode makes sense. If it
// doesn't, we choose a better preferred mode.
void Disman::fix_InvalidMode(json &disconfig) const {
    using std::string;
    using std::vector;

    for (auto& gpu : disconfig["gpus"]) {
        if ((gpu["desktop_type"].get<string>() == "cloned") && (gpu["displays"].size() > 1)) {
            vector<json> v;
            json max_mode;

            // Find list of valid modes for all non panel fitter displays and
            // maximum mode allowed for all panel fitter displays
            for (auto& display : gpu["displays"]) {
                if (display["modes"].size() == 1) {
                    if (max_mode.empty()) {
                        max_mode = display["modes"].at(0);
                    } else {
                        if (modeCmp(display["modes"].at(0), max_mode)) {
                            max_mode = display["modes"].at(0);
                        }
                    }

                    qDebug() << display["name"] << "-" << display["preferred_mode_index"] << display["modes"];
                    continue;
                }
                if (v.empty()) {
                    qDebug() << display["name"] << "-" << display["preferred_mode_index"] << display["modes"];
                    v = display["modes"].get<vector<json>>();
                    std::sort(v.begin(), v.end(), modeCmp);
                } else {
                    qDebug() << display["name"] << "-" << display["preferred_mode_index"] << display["modes"];
                    vector<json> v1 = v;
                    vector<json> v2 = display["modes"];
                    std::sort(v2.begin(), v2.end(), modeCmp);

                    v.clear();
                    std::set_intersection(v1.begin(), v1.end(),
                                          v2.begin(), v2.end(),
                                          std::back_inserter(v),
                                          modeCmp);
                }
                qDebug() << v;
            }

            // Ensure all panel fitter displays are set to the only mode available
            for (auto& display : gpu["displays"]) {
                if (display["modes"].size() == 1) {
                    display["preferred_mode_index"] = 0;
                }
            }

            // If we have non panel fitter displays, let's deal with them by
            // fixing up some things with our vector.
            if (!v.empty()) {
                // The vector is in lowest resolution first order, we want the opposite.
                std::reverse(v.begin(), v.end());
                // Eliminate modes that are larger than the smallest display with panel fitter
                if (!max_mode.empty()) {
                    for (auto it = v.begin(); it != v.end(); ) {
                        if ((*it)["width"] > max_mode["width"] || (*it)["height"] > max_mode["height"]) {
                            it = v.erase(it);
                        } else {
                            ++it;
                        }
                    }
                }

                // I refuse to accept that there is any combination of displays
                // out there that will result in an empty vector at this point.
                // If there are, the only recourse that I would have is total
                // world destruction. Since I'm not sure how to pull that off,
                // I'll just log the situation and pretend that it didn't happen.
                if (!v.empty()) {
                    json mode;
                    // Find previous mode if all non panel fitter displays agree
                    for (auto& display : gpu["displays"]) {
                        if (display["modes"].size() == 1) {
                            continue;
                        }

                        if (mode.empty()) {
                            if (display["preferred_mode_index"] < display["modes"].size()) {
                                mode = display["modes"].at(display["preferred_mode_index"].get<int>());
                                if (std::find(v.begin(), v.end(), mode) != v.end()) {
                                    continue;
                                }
                            }
                        } else {
                            if (display["preferred_mode_index"] < display["modes"].size() &&
                                mode == display["modes"].at(display["preferred_mode_index"].get<int>())) {
                                continue;
                            }
                        }

                        // If we get here, we have a problem, but it's fixable.
                        mode.clear();
                        qWarning() << "Invalid preferred mode, Defaulting to the first valid mode.";
                        break;
                    }

                    // Decide on a new preferred mode and make sure all non panel fitter displays are set to it
                    if (mode.empty()) {
                        mode = v[0];

                        // Fix all non panel fitter displays to match it
                        for (auto& display : gpu["displays"]) {
                            if (display["modes"].size() != 1) {
                                vector<json> modes = display["modes"];
                                auto iter = std::find(modes.begin(), modes.end(), mode);
                                display["preferred_mode_index"] = std::distance(modes.begin(), iter);
                            }
                        }
                    }
                } else {
                    qCritical() << "No valid modes remain for cloned mode.";
                }
            }
        } else {
            for (auto& display : gpu["displays"]) {
                if (display["preferred_mode_index"] >= display["modes"].size()) {
                    qWarning() << "Invalid preferred mode, Defaulting to the first valid mode.";
                    display["preferred_mode_index"] = 0;
                }
            }
        }
    }
}

// Filter overlapping displays - This invalidates the coordinates for the
//    overlapping display. The coordinates will be fixed later in
//    fix_InvalidPositions()
void Disman::filter_OverlappingDisplays(json &disconfig) const {
    using std::string;
    QRegion desktop;

    for (auto& gpu : disconfig["gpus"]) {
        for (auto& display : gpu["displays"]) {
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) { continue; }
            QSize resolution = getDisplayResolution(display);
            if (resolution == INVALIDSIZE) { continue; }
            QRect rect = QRect(position, resolution);

            if (desktop.contains(rect)) {
                qWarning() << "Overlapping display detected. Display position invalid:" << display;
                display["x"] = MAX_NEG_INT;
                display["y"] = MAX_NEG_INT;
            } else {
                desktop += rect;
            }
            if (gpu["desktop_type"].get<string>() == "cloned") { break; }
        }
    }
}

// Filter orphaned displays - This invalidates the coordinates for displays that
//    are not touching another displays edge. The coordinates will be fixed
//    later in fix_InvalidPositions()
void Disman::filter_OrphanedDisplays(json &disconfig) const {
    using std::string;
    using std::reference_wrapper;

    QRegion desktop;
    json initialDisplay;
    QRect initialRect;
    QList<reference_wrapper<json>> unfound;

    // Find all displays that we care about
    for (auto& gpu : disconfig["gpus"]) {
        for (auto& display : gpu["displays"]) {
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) { continue; }
            QSize resolution = getDisplayResolution(display);
            if (resolution == INVALIDSIZE) { continue; }
            QRect rect = QRect(position, resolution);

            if ((initialDisplay.empty()) ||
                (rect.left() <= initialRect.left() && rect.top() <= initialRect.left())) {
                unfound.push_front(reference_wrapper<json>(display));
                initialRect = rect;
                initialDisplay = display;
            } else {
                unfound.push_back(reference_wrapper<json>(display));
            }
            if (gpu["desktop_type"].get<string>() == "cloned") { break; }
        }
    }

    if (!unfound.isEmpty()) {
        unfound.removeFirst();
    }

    if (unfound.isEmpty()) { return; }

    desktop += initialRect;

    // This will remove all the displays that are connected and thus unfound
    // will be left with just those that are orphaned
    // Loop through unfound until either there are no displays left or the size
    // doesn't change
    for (auto size = -1;0 != unfound.size() && unfound.size() != size;) {
        size = unfound.size();
        // Find a way to redo this so that we can get rid of the foreach
        // render_stack.remove_if([&](std::shared_ptr<vm_region_t> &v) { return v->uuid() == vm->uuid(); });
        foreach (auto &rw, unfound) {
            json& display = rw.get();
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) { continue; }
            QSize resolution = getDisplayResolution(display);
            if (resolution == INVALIDSIZE) { continue; }
            QRect rect1 = QRect(position, resolution);

            for (const auto &rect2 : desktop) {
                // We already checked the rects for overlapping.
                if (!(rect1.left()   > rect2.right()  + 1 ||
                      rect1.right()  < rect2.left()   - 1 ||
                      rect1.top()    > rect2.bottom() + 1 ||
                      rect1.bottom() < rect2.top()    - 1)) {
                    desktop += rect1;
                    unfound.removeAll(rw);
                    break;
                }
            }
        }
    }

    // do we have any orphaned displays
    if (unfound.size() > 0) {
        // Loop through those left unfound and make the position invalid
        // these positions will get fixed later when fix_InvalidPositions()
        // is called
        for (auto &rw : unfound) {
            json& display = rw.get();
            qWarning() << "Orphaned display detected. Display position invalid:" << display;
            display["x"] = MAX_NEG_INT;
            display["y"] = MAX_NEG_INT;
        }
    }
}

// Fix non-origin Desktop - Ensure that the desktop (not necessarily the
//    left most/top most display) starts at our origin location (0,0). All
//    displays are shifted by the same amount to get all coordinates in the
//    positive range.
///////////////////////////////////////////////////////////////////////////////
//
//    • = (0,0)
//
///////////////////////////////////////////////////////////////////////////////
//
//    •                                             •   ---------
//                                                      |   |   |
//        ---------                                 -------------
//        |   |   |                                 |   |   |   |
//    -------------            ====>>>>>            -------------
//    |   |   |   |
//    -------------
//
///////////////////////////////////////////////////////////////////////////////
//
//    ---------                                     •--------
//    |   |   |                                     |   |   |
//    ----•--------            ====>>>>>            -------------
//    |   |   |   |                                 |   |   |   |
//    -------------                                 -------------
//
///////////////////////////////////////////////////////////////////////////////
void Disman::fix_NonOriginDesktop(json &disconfig) const {
    using std::string;
    QRegion desktop;

    // Build the desktop region
    for (auto& gpu : disconfig["gpus"]) {
        for (auto& display : gpu["displays"]) {
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) { continue; }
            QSize resolution = getDisplayResolution(display);
            if (resolution == INVALIDSIZE) { continue; }
            QRect rect = QRect(position, resolution);

            desktop += rect;
            if (gpu["desktop_type"].get<string>() == "cloned") { break; }
        }
    }

    QPoint delta = desktop.boundingRect().topLeft();
    if (delta != QPoint(0, 0)) {
        qWarning() << "Non-Origin desktop detected. Adjusting all display positions to fix!!!";

        // Adjust all non-invalid displays by the delta
        for (auto& gpu : disconfig["gpus"]) {
            for (auto& display : gpu["displays"]) {
                QPoint position = getDisplayPosition(display);
                if (position == INVALIDPOINT) { continue; }

                display["x"] = display["x"].get<int>() - delta.x();
                display["y"] = display["y"].get<int>() - delta.y();
                if (gpu["desktop_type"].get<string>() == "cloned") { break; }
            }
        }
    }
}

// Fix invalid positions - Position displays that have invalid coordinates. The
//    new location is predictable. It'll line up with the top edge of the
//    rightmost/topmost display.
//
///////////////////////////////////////////////////////////////////////////////
//
//    -------------                                 ---------------------
//    |   |   |   |                                 |   |   |   | X | Y |
//    -------------                                 ---------------------
//    |   |   |                                     |   |   |
//    ---------                ====>>>>>            ---------
//
//    -----
//    | X | <<<<==== This display has it's x and y set to MAX_NEG_INT
//    -----
//
//    -----
//    | Y | <<<<==== This display has it's x and y set to MAX_NEG_INT
//    -----
//
///////////////////////////////////////////////////////////////////////////////
//
//    ---------                                     ---------
//    |   |   |                                     |   |   |
//    -------------                                 ---------------------
//    |   |   |   |                                 |   |   |   | X | Y |
//    -------------            ====>>>>>            ---------------------
//
//    -----
//    | X | <<<<==== This display has it's x and y set to MAX_NEG_INT
//    -----
//
//    -----
//    | Y | <<<<==== This display has it's x and y set to MAX_NEG_INT
//    -----
//
///////////////////////////////////////////////////////////////////////////////
void Disman::fix_InvalidPositions(json &disconfig) const {
    using std::string;
    QRegion desktop;

    // Build the region from non-invalid displays
    for (auto& gpu : disconfig["gpus"]) {
        for (auto& display : gpu["displays"]) {
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) { continue; }
            QSize resolution = getDisplayResolution(display);
            if (resolution == INVALIDSIZE) { continue; }
            QRect rect = QRect(position, resolution);

            if (desktop.contains(rect) == false) { desktop += rect; }
            if (gpu["desktop_type"].get<string>() == "cloned") { break; }
        }
    }

    // Find the next best location for first invalid display
    int32_t x = 0;
    int32_t y = 0;
    if (!desktop.isEmpty())
    {
        auto boundingRect = desktop.boundingRect();

        x = boundingRect.right() + 1;
        y = boundingRect.top();

        for (; y < boundingRect.bottom(); y++)
        {
            if (desktop.contains(QPoint(x - 1, y))) { break; }
        }
    }

    // Place each invalid display at the next best location
    for (auto& gpu : disconfig["gpus"]) {
        for (auto& display : gpu["displays"]) {
            QPoint position = getDisplayPosition(display);
            if (position == INVALIDPOINT) {
                QSize resolution = getDisplayResolution(display);
                if (resolution == INVALIDSIZE) { continue; }

                display["x"] = x;
                display["y"] = y;
                x += resolution.width();
            }
            if (gpu["desktop_type"].get<string>() == "cloned") { break; }
        }
    }
}
