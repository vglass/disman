//
// Disman
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <stdexcept>    // std::exception
#include <sstream>      // std::stringstream, std::endl
#include <string>       // std::string
#include <cstdint>      // int32_t
#include <QtCore>
#include <json.hpp>
#include <valijson/adapters/nlohmann_json_adapter.hpp>
#include <valijson/utils/nlohmann_json_utils.hpp>
#include <valijson/schema.hpp>
#include <valijson/schema_parser.hpp>
#include <valijson/validation_results.hpp>
#include <valijson/validator.hpp>

#include <disman.h>
#include <qdebug_helper.h>

// ============================================================================
// Implementation
// ============================================================================

// RCF - Need try/catch throughout this
bool Disman::validateConfig(json &disconfig, QString schemaFile)
{
    using std::stringstream;
    using std::string;
    using std::vector;
    using std::endl;
    using valijson::Schema;
    using valijson::SchemaParser;
    using valijson::Validator;
    using valijson::ValidationResults;
    using valijson::adapters::NlohmannJsonAdapter;

    if (disconfig.empty()) { return false; }
    if (schemaFile.isEmpty()) { return false; }
    if (!QFile::exists(schemaFile)) {
        schemaFile = QString("/usr/share/disman/") + schemaFile;
    }
    if (!QFile::exists(schemaFile)) { return false; }

    json schemaDoc;
    if (!valijson::utils::loadDocument(schemaFile.toStdString(), schemaDoc)) {
        qWarning() << "Failed to load schema from" << schemaFile;
        return false;
    }

    Schema schema;
    SchemaParser parser;
    NlohmannJsonAdapter schemaAdapter(schemaDoc);
    parser.populateSchema(schemaAdapter, schema);
    try {
        parser.populateSchema(schemaAdapter, schema);
    }
    catch (std::exception &e) {
        qWarning() << "Failed to parse schema: " << e.what();
        return false;
    }

    NlohmannJsonAdapter doc(disconfig);
    Validator validator(Validator::kStrongTypes);
    ValidationResults results;
    if (!validator.validate(schema, doc, &results)) {
        stringstream msg;
        msg << "Validation failed." << endl;
        ValidationResults::Error error;
        int32_t error_num = 1;
        while (results.popError(error)) {
            string context;
            for (const auto& s : error.context) {
                context += s;
            }

            msg << "Error #" << error_num << endl
                << "  context: " << context << endl
                << "  desc:    " << error.description << endl;
            ++error_num;
        }
        qWarning() << msg;
        return false;
    }
    return true;
}

