//
// DBus Plugin
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <memory>       // std::shared_ptr
#include <QtCore>
#include <QDBusConnection>
#include <QDBusAbstractAdaptor>

#include <dbus.h>

// ============================================================================
// Implementation
// ============================================================================

DBus::DBus(void) : sysBus(QDBusConnection::systemBus()) {}
DBus::~DBus(void) {}

QDBusConnection& DBus::systemBus() {
    return sysBus;
}

bool DBus::registerObject(QObject* obj, const QString &object_name) {
    if (!isConnected()) {
        return false;
    }

    // Register our adaptor at the object path
    if (!sysBus.registerObject(object_name, obj)) {
        qWarning().nospace() << "Failed to register DBUS object at " << object_name << ".";
        return false;
    }

    return true;
}

bool DBus::unregisterService(const QString &service_name) {
    if (!isConnected()) {
        return false;
    }

    if (!sysBus.unregisterService(service_name)) {
        return false;
    }

    if (1 != dbusAdaptors.remove(service_name)) {
        return false;
    }

    return true;
}

bool DBus::unregisterObject(const QString &object_name) {
    if (!isConnected()) {
        return false;
    }

    sysBus.unregisterObject(object_name);

    return true;
}

bool DBus::isConnected() const {
    if (!sysBus.isConnected()) {
        qWarning() << "Not connected to QDBusConnection::systemBus().";
        return false;
    }

    return true;
}

bool DBus::registerService(const std::shared_ptr<QDBusAbstractAdaptor> dbusAdaptor, const QString &service_name) {
    // Check connection to the system bus
    if (!isConnected()) {
        return false;
    }

    dbusAdaptors.insert(service_name, dbusAdaptor);

    if (!sysBus.registerService(service_name)) {
        qWarning().nospace() << "Failed to register DBUS service for " << service_name << ".";
        return false;
    }

    return true;
}
