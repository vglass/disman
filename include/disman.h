#ifndef DISMAN_H
#define DISMAN_H

//
// Disman
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <memory>       // std::unique_ptr
#include <limits>       // std::numeric_limits
#include <cstdint>      // int32_t
#include <QtCore>
#include <QTimer>
#include <json.hpp>

#include <dbus.h>
#include <config.h>
#include <dedicatedresource.h>
#include <vglass_interface.h>

#include <dbus_listener.h>

extern "C" {
    #include <pv_display_backend_helper.h>
}

// ============================================================================
// Macros / Constants
// ============================================================================

const int32_t MAX_NEG_INT = std::numeric_limits<int32_t>::min();
const QPoint INVALIDPOINT = QPoint(MAX_NEG_INT, MAX_NEG_INT);
const QSize INVALIDSIZE = QSize(MAX_NEG_INT, MAX_NEG_INT);
const QString PIDFILE = QString("/var/run/disman.pid");

// ============================================================================
// Definition
// ============================================================================

class db_dbus_t;
class Disman : public QObject {
    Q_OBJECT

public:
    Disman(QString configLocation, bool testModeEnabled);
    virtual ~Disman();

public slots:

    // Used by UIVM
    virtual QString loadConfig(void);
    virtual bool saveConfig(const QString &config, QString &result);
    virtual int32_t getBannerHeight() const;
    virtual void setBannerHeight(const uint32_t value);
    virtual int32_t getFrameTime() const;
    virtual void setFrameTime(const uint32_t value);
    // Used by displayhandler-hotplug.sh
    virtual void hotplug(void);
    // Used by VGlass
    virtual void refresh(const bool mustRespond = true);
    virtual void handle_hotplug();
    virtual void revertToClonedMode();
    virtual void removeSavedConfig(QString hash) const { removeSavedConfigs(hash); };
    virtual void removeAllSavedConfigs() const { removeSavedConfigs(); };
    // Used by IVC / NXUS
    virtual void guest_started(const uuid_t& uuid);
    virtual void guest_stopped(const uuid_t& uuid);
    virtual void guest_deleted(const uuid_t& uuid);
    virtual void displays_changed(const uuid_t& uuid, const QList<dh_display_info>& display_info, const uint32_t size);

    virtual void exit();

signals:
    // Used by UIVM
    void configChanged(QString config);

private:
    virtual void registerDbus();
    virtual void unregisterDbus();

    virtual void nxus_init();
    virtual void nxus_deinit();
    virtual QString getRandomConfig();
    virtual json getConfig();
    virtual bool validateConfig(json &disconfig, QString schemaFile);
    virtual void loadConfig(json &disconfig);
    virtual bool saveConfig(json &disconfig, const bool mustRespond);
    virtual uint32_t calculateDismanHash(json &disconfig);
    virtual uint32_t calculateGpuHash(json &gpu);
    virtual uint32_t calculateDisplayHash(json &display);
    virtual QString debugDump(const json &disconfig, const QString tag) const;

    virtual bool refresh(json &disconfig, const bool mustRespond = false);
    virtual void removeSavedConfigs(QString hash = "") const;

    static bool modeCmp(const json &a, const json &b);

    virtual QPoint getDisplayPosition(json &display) const;
    virtual QSize getDisplayResolution(json &display) const;

    virtual void fix_InvalidCloned(json &disconfig) const;
    virtual void fix_InvalidPinned(json &disconfig) const;
    virtual void fix_MissingShared(json &disconfig) const;
    virtual void fix_InvalidMode(json &disconfig) const;
    virtual void filter_OverlappingDisplays(json &disconfig) const;
    virtual void filter_OrphanedDisplays(json &disconfig) const;
    //virtual void filter_SplitDesktop(json &disconfig) const;
    virtual void fix_NonOriginDesktop(json &disconfig) const;
    virtual void fix_InvalidPositions(json &disconfig) const;

private:
    json mConfig; // Needs to be held onto in case of a loadConfig() call and for NXUS
    std::unique_ptr<db_dbus_t> db;
    std::unique_ptr<DBus> dbus;
    std::unique_ptr<Config> config;
    //std::unique_ptr<com::openxt::vglass> vglass;
    QString mConfigLocation;
    bool mTestModeEnabled;
    bool mDismanReady;
    QTimer *mTimer;

    std::shared_ptr<dbus_listener_t> mDBusListener;
    QMap<uuid_t, std::shared_ptr<DedicatedResource>> mDedicatedResources;
};

#endif // DISMAN_H
