#ifndef CONFIG_H
#define CONFIG_H

//
// Config Plugin
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <memory>       // std::unique_ptr
#include <QtCore>
#include <json.hpp>

// ============================================================================
// Definition
// ============================================================================

class db_dbus_t;
class Config : public QObject
{
    Q_OBJECT

    using json = nlohmann::json;

public:
    Config();
    virtual ~Config();

    static json parse(const QString config);

    virtual bool exists(const QUrl url) const;
    virtual bool save(const QUrl url, const json &config) const;
    virtual json load(const QUrl url) const;
    virtual bool remove(const QUrl url) const;

private:
    virtual bool saveConfigToDisk(const QString path, const json &config) const;
    virtual bool saveConfigToDb(const QString path, const json &config) const;
    virtual json loadConfigFromDisk(const QString path) const;
    virtual json loadConfigFromDb(const QString path) const;

private:
    std::unique_ptr<db_dbus_t> db;
};

#endif // CONFIG_H
