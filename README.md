---

DBus configuration for Disman 2.0  

---

If you are trying to get this to run on a non-openxt based box, you'll need to install the DBus config file.

sudo cp -f dbus-1/com.openxt.disman.conf /etc/dbus-1/system.d/  
sudo /etc/init.d/dbus restart

